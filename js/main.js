/**
 * Array of objects in the game
 *
 * @type {Array}
 */
var objects = [
    {"name": "rock"},
    {"name": "paper"},
    {"name": "scissors"}
];

/**
 * Object lookup to give the array index possition of an object given the name
 *
 * @type {Object}
 */
var objectIndexLookup = {
    "rock": 0,
    "paper": 1,
    "scissors": 2
};

var humanPlayerName = "user";
var computerPlayerName = "computer";

var RPSViewModel = function() {
    var self = this;
    self.userChoice = ko.observable();
    self.userObjectSrc = ko.observable();
    self.userScore = ko.observable(0);
    self.ties = ko.observable(0);
    self.numberOfGames = ko.observable(0);
    self.computerChoice = ko.observable();
    self.computerObjectSrc = ko.observable();
    self.computerScore = ko.observable(0);
    self.result = ko.observable();
    self.resultMessage = ko.observable();
    self.resultHistory = ko.observableArray();

    /**
     * The function triggered when a user clicks on an object
     *
     * @param  {Object} data         DOM provded data object
     * @param  {Object} event        DOM provded event object
     * @param  {String} chosenObject The object selected by the user
     * @return {null}
     */
    self.clickOnObject = function(data, event, chosenObject) {
        self.numberOfGames(self.numberOfGames() + 1);
        self.userChoice(chosenObject);
        self.computerChoice(getComputerChoice(objects).name);
        //The image sources of the objects
        self.userObjectSrc("img/" + self.userChoice() + ".png");
        self.computerObjectSrc("img/" + self.computerChoice() + ".png");

        var determineWinner = whoWins(self.userChoice(), self.computerChoice());
        var resultMessage;

        //Tally up scores and populate result messages
        if (determineWinner.winner === humanPlayerName) {
            self.userScore(self.userScore() + 1);
            resultMessage = "You win as " + determineWinner.msg;
        } else if (determineWinner.winner === computerPlayerName) {
            self.computerScore(self.computerScore() + 1);
            resultMessage = "Computer wins as " + determineWinner.msg;
        } else {
            self.ties(self.ties() + 1);
            resultMessage = determineWinner.msg;
        }
        self.resultMessage(resultMessage);
        self.resultHistory.push(
            {
                "result": "You chose: " + self.userChoice() +
                 ", the computer chose: " + self.computerChoice() + ". " +
                 resultMessage + "."
            }
        );
    };
};

/**
 * Given two players object choice, this function determines who the winner is
 * or if a draw has occured.
 * A 2D array is used to determine the winner where the first index is the 'human'
 * player and the second index is the computer player. The table below displays
 * all cases (where U is user and C is computer).
 *
 * +----------+------------------------------------+
 * |          | Computer (C)                       |
 * +----------+------------------------------------+
 * | User (U) |          | Rock | Paper | Scissors |
 * +          +----------+------+-------+----------+
 * |          | Rock     | T    | C     | U        |
 * +          +----------+------+-------+----------+
 * |          | Paper    | U    | T     | C        |
 * +          +----------+------+-------+----------+
 * |          | Scissors | C    | U     | T        |
 * +----------+----------+------+-------+----------+
 *
 * @param  {String} userObjectChoice     The name of the object chosen by this player
 * @param  {String} computerObjectChoice The name of the object chosen by this player
 * @return {Object}                      Result object containing winner and message
 */
var whoWins = function(userObjectChoice, computerObjectChoice) {
    var TIE_MESSAGE = "It's a tie";
    var PAPER_COVERS_ROCK_MESSAGE = "paper covers rock";
    var ROCK_SMASHES_SCISSORS_MESSAGE = "rock smashes scissors";
    var SCISSORS_CUTS_PAPER_MESSAGE = "scissors cuts paper";

    var cases = [
        [
            {"winner": null, "msg": TIE_MESSAGE},
            {"winner": computerPlayerName, "msg": PAPER_COVERS_ROCK_MESSAGE},
            {"winner": humanPlayerName, "msg": ROCK_SMASHES_SCISSORS_MESSAGE}
        ],
        [
            {"winner": humanPlayerName, "msg": PAPER_COVERS_ROCK_MESSAGE},
            {"winner": null, "msg": TIE_MESSAGE},
            {"winner": computerPlayerName, "msg": SCISSORS_CUTS_PAPER_MESSAGE}
        ],
        [
            {"winner": computerPlayerName, "msg": ROCK_SMASHES_SCISSORS_MESSAGE},
            {"winner": humanPlayerName, "msg": SCISSORS_CUTS_PAPER_MESSAGE},
            {"winner": null, "msg": TIE_MESSAGE}
        ]
    ];
    return cases[objectIndexLookup[userObjectChoice]][objectIndexLookup[computerObjectChoice]];
};

/**
 * Dermines what object to draw when called given an array of objects.
 * Currently the object is chosen by random.
 *
 * @param  {Array} objects  Array of objects (rock, paper or scissors)
 * @return {Object}         The chosen object from the array
 */
var getComputerChoice = function(objects) {
    return objects[Math.floor(Math.random() * objects.length)];
};

ko.applyBindings(new RPSViewModel(), document.getElementById("htmlTop"));
