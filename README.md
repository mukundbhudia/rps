# rps - Rock, Paper, Scissors #

### What is this? ###

Challenge:

* Write the game: rock, paper, scissors using any javascript web framework.
* Include all instructions to setup and run, and a script to do the same.
* The user should be able to run one command and then view the app.
* Then, please write a script that installs all requirements and launches as requested. (This is treated as the ability to understand and complete instructions).
* No instructions to read documentation are required, just a script to run the project will suffice.

### What you need before you run the app: ###

* A modern web browser

### How do I run it? ###

Download the repo. Open index.html in a web browser.

### Is there a demo? ###

https://mukundbhudia.gitlab.io/rps/

### Known issues ###

None yet
